﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Grpc.Core;

namespace Client
{
    class Program
    {
        static void Main(string[] args)
        {
            const string Host = "localhost";
            const int Port = 16842;

            var channel = new Channel($"{Host}:{Port}", ChannelCredentials.Insecure);

            Console.WriteLine("Enter your name :");
            var name = Console.ReadLine();
            Console.WriteLine("Enter your birthday :");
            var date = Console.ReadLine();

            bool validDate = false;

            do
            {
                DateTime temp;
                if (DateTime.TryParseExact(date, "dd.MM.yyyy", CultureInfo.InvariantCulture,
                       DateTimeStyles.None, out temp))
                {
                    validDate = true;
                    date = temp.ToString();
                }
                else
                {
                    Console.WriteLine("Your birthday is INVALID");
                    Console.WriteLine("Enter your birthday :");
                    date = Console.ReadLine();
                }
            } while (!validDate);

            var connect_stub = new Generated.ConnectService.ConnectServiceClient(channel);

            var connectResponse = connect_stub.Connect(new Generated.ConnectRequest
            {
                Name = name
            });
            
            var signs_stub = new Generated.SignsService.SignsServiceClient(channel);
            
            var signsResponse = signs_stub.Sign(new Generated.SignsRequest
            {
                Date = date
            });

            Console.WriteLine($"Your sign is: {signsResponse.Sign}");

            // Shutdown
            channel.ShutdownAsync().Wait();
            Console.WriteLine("Client closed");
            Console.ReadKey();
        }
    }
}
