// <auto-generated>
//     Generated by the protocol buffer compiler.  DO NOT EDIT!
//     source: SummerSignsService.proto
// </auto-generated>
#pragma warning disable 1591, 0612, 3021
#region Designer generated code

using pb = global::Google.Protobuf;
using pbc = global::Google.Protobuf.Collections;
using pbr = global::Google.Protobuf.Reflection;
using scg = global::System.Collections.Generic;
namespace Generated {

  /// <summary>Holder for reflection information generated from SummerSignsService.proto</summary>
  public static partial class SummerSignsServiceReflection {

    #region Descriptor
    /// <summary>File descriptor for SummerSignsService.proto</summary>
    public static pbr::FileDescriptor Descriptor {
      get { return descriptor; }
    }
    private static pbr::FileDescriptor descriptor;

    static SummerSignsServiceReflection() {
      byte[] descriptorData = global::System.Convert.FromBase64String(
          string.Concat(
            "ChhTdW1tZXJTaWduc1NlcnZpY2UucHJvdG8aElNpZ25zUmVxdWVzdC5wcm90",
            "bxoTU2lnbnNSZXNwb25zZS5wcm90bzJBChJTdW1tZXJTaWduc1NlcnZpY2US",
            "KwoKU3VtbWVyU2lnbhINLlNpZ25zUmVxdWVzdBoOLlNpZ25zUmVzcG9uc2VC",
            "DKoCCUdlbmVyYXRlZGIGcHJvdG8z"));
      descriptor = pbr::FileDescriptor.FromGeneratedCode(descriptorData,
          new pbr::FileDescriptor[] { global::Generated.SignsRequestReflection.Descriptor, global::Generated.SignsResponseReflection.Descriptor, },
          new pbr::GeneratedClrTypeInfo(null, null, null));
    }
    #endregion

  }
}

#endregion Designer generated code
