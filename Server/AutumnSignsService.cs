﻿using Generated;
using Grpc.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    class AutumnSignsService : Generated.AutumnSignsService.AutumnSignsServiceBase
    {
        enum AutumnSigns
        {
            Libra,
            Scorpio,
            Sagittarius,
            InvalidAutumnSign
        }

        private AutumnSigns GetSign(string date)
        {
            if (Utils.Between(date, 23, 9, 22, 10))
                return AutumnSigns.Libra;
            if (Utils.Between(date, 23, 10, 21, 11))
                return AutumnSigns.Scorpio;
            if (Utils.Between(date, 22, 11, 21, 12))
                return AutumnSigns.Sagittarius;
            return AutumnSigns.InvalidAutumnSign;
        }
        public override Task<SignsResponse> AutumnSign(SignsRequest request, ServerCallContext context)
        {
            AutumnSigns sign = GetSign(request.Date);
            return Task.FromResult(new SignsResponse { Sign = sign.ToString() });
        }
    }
}
