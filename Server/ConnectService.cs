﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Generated;
using Grpc.Core;

namespace Server
{
    internal class ConnectService : Generated.ConnectService.ConnectServiceBase
    {
        public override Task<global::Google.Protobuf.WellKnownTypes.Empty> Connect(ConnectRequest request, ServerCallContext context)
        {
            System.Console.WriteLine($"User with name {request.Name} connected");
            return Task.FromResult(new global::Google.Protobuf.WellKnownTypes.Empty());
            //return base.Connect(request, context);
        }
    }
}
