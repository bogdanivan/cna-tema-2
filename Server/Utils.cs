﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    public static class Utils
    {
        public static bool Between(string date, int day1, int month1, int day2, int month2, bool overflow = false)
        {
            var tokens = date.Split('/');
            var month = int.Parse(tokens[0]);
            var day = int.Parse(tokens[1]);
            var year = int.Parse(tokens[2].Substring(0,4));

            DateTime current = new DateTime(year, month, day);
            DateTime second = new DateTime(year, month2, day2);
            if (overflow)
                year--;
            DateTime first = new DateTime(year, month1, day1);

            if (first <= current && current <= second)
                return true;
            return false;
        }
    }
}
