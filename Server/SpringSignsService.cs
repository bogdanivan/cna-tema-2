﻿using Generated;
using Grpc.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    class SpringSignsService : Generated.SpringSignsService.SpringSignsServiceBase
    {
        enum SpringSigns
        {
            Aries,
            Taurus,
            Gemini,
            InvalidSpringSign
        }

        private SpringSigns GetSign(string date)
        {
            if (Utils.Between(date, 21, 3, 19, 4))
                return SpringSigns.Aries;
            if (Utils.Between(date, 20, 4, 20, 5))
                return SpringSigns.Taurus;
            if (Utils.Between(date, 21, 5, 20, 6))
                return SpringSigns.Gemini;
            return SpringSigns.InvalidSpringSign;
        }
        public override Task<SignsResponse> SpringSign(SignsRequest request, ServerCallContext context)
        {
            SpringSigns sign = GetSign(request.Date);
            return Task.FromResult(new SignsResponse { Sign = sign.ToString() });
        }
    }
}
