﻿using Generated;
using Grpc.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    class WinterSignsService : Generated.WinterSignsService.WinterSignsServiceBase
    {
        enum WinterSigns
        {
            Capricorn,
            Aquarius,
            Pisces,
            InvalidWinterSign
        }

        private WinterSigns GetSign(string date)
        {
            if (Utils.Between(date, 21, 12, 19, 1, true))
                return WinterSigns.Capricorn;
            if (Utils.Between(date, 20, 1, 18, 2))
                return WinterSigns.Aquarius;
            if (Utils.Between(date, 19, 2, 20, 3))
                return WinterSigns.Pisces;
            return WinterSigns.InvalidWinterSign;
        }
        public override Task<SignsResponse> WinterSign(SignsRequest request, ServerCallContext context)
        {
            WinterSigns sign = GetSign(request.Date);
            return Task.FromResult(new SignsResponse { Sign = sign.ToString() });
        }
    }
}
