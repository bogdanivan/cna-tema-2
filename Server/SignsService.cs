﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Generated;
using Grpc.Core;

namespace Server
{
    internal class SignsService : Generated.SignsService.SignsServiceBase
    {
        enum Seasons
        {
            Winter,
            Spring,
            Summer,
            Autumn,
            InvalidSeason
        }

        private Seasons GetSeasons(string date)
        {  
            if (Utils.Between(date, 21, 3, 20, 6))
                return Seasons.Spring;
            if (Utils.Between(date, 21, 6, 22, 9))
                return Seasons.Summer;
            if (Utils.Between(date, 23, 9, 21, 12))
                return Seasons.Autumn;
            if (Utils.Between(date, 21, 12, 20, 3, true))
                return Seasons.Winter;
            return Seasons.InvalidSeason;
        }
        
        public override Task<SignsResponse> Sign(SignsRequest request, ServerCallContext context)
        {
            var season = GetSeasons(request.Date);
            var channel = new Channel($"{Configuration.HOST}:{Configuration.PORT}", ChannelCredentials.Insecure);
            var summer_stub = new Generated.SummerSignsService.SummerSignsServiceClient(channel);
            var winter_stub = new Generated.WinterSignsService.WinterSignsServiceClient(channel);
            var autumn_stub = new Generated.AutumnSignsService.AutumnSignsServiceClient(channel);
            var spring_stub = new Generated.SpringSignsService.SpringSignsServiceClient(channel);
            string sign = string.Empty;
            switch (season)
            {
                case Seasons.Winter:
                    var winterResponse = winter_stub.WinterSign(new Generated.SignsRequest
                    {
                        Date = request.Date
                    });
                    sign = winterResponse.Sign;
                    break;
                case Seasons.Autumn:
                    var autumnResponse = autumn_stub.AutumnSign(new Generated.SignsRequest
                    {
                        Date = request.Date
                    });
                    sign = autumnResponse.Sign;
                    break;
                case Seasons.Summer:
                    var summerResponse = summer_stub.SummerSign(new Generated.SignsRequest
                    {
                        Date = request.Date
                    });
                    sign = summerResponse.Sign;
                    break;
                case Seasons.Spring:
                    var springResponse = spring_stub.SpringSign(new Generated.SignsRequest
                    {
                        Date = request.Date
                    });
                    sign = springResponse.Sign;
                    break;
                case Seasons.InvalidSeason:
                    sign = "Invalid";
                    break;
                default:
                    break;
            }
            return Task.FromResult(new SignsResponse {Sign = sign });
        }
    }
}
