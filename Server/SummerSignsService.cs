﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Generated;
using Grpc.Core;

namespace Server
{
    internal class SummerSignsService : Generated.SummerSignsService.SummerSignsServiceBase
    {
        enum SummerSigns
        {
            Cancer,
            Leo,
            Virgo,
            InvalidSummerSign
        }

        private SummerSigns GetSign(string date)
        {
            if (Utils.Between(date, 21, 6, 22, 7)) 
                return SummerSigns.Cancer;
            if (Utils.Between(date, 23, 7, 22, 8))
                return SummerSigns.Leo;
            if (Utils.Between(date, 23, 8, 22, 9))
                return SummerSigns.Virgo;
            return SummerSigns.InvalidSummerSign;
        }
        public override Task<SignsResponse> SummerSign(SignsRequest request, ServerCallContext context)
        {
            SummerSigns sign = GetSign(request.Date);
            return Task.FromResult(new SignsResponse { Sign = sign.ToString() });
        }
    }
}
